// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"gitlab.com/redfield/redctl/api"
)

// ImageCreate to create an image
func (c *Client) ImageCreate(name string, size int64) (*api.Image, error) {
	r := api.ImageCreateRequest{
		Image: &api.Image{Name: name, Size: size},
	}

	reply, err := c.redctlClient.ImageCreate(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return reply.GetImage(), nil
}

// ImageCreateBacked to create an image backed by another image
func (c *Client) ImageCreateBacked(name string, size int64, backingImageName string) (*api.Image, error) {
	r := api.ImageCreateBackedRequest{
		Image:        &api.Image{Name: name, Size: size},
		BackingImage: &api.Image{Name: backingImageName},
	}

	reply, err := c.redctlClient.ImageCreateBacked(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return reply.GetImage(), nil
}

// ImageFind to find an image
func (c *Client) ImageFind(name string) (*api.Image, error) {
	r := api.ImageFindRequest{
		Name: name,
	}

	d, err := c.redctlClient.ImageFind(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	images := d.GetImages()
	if images == nil {
		return nil, nil
	}

	return d.GetImages()[0], nil
}

// ImageFindAll to find all images
func (c *Client) ImageFindAll() ([]*api.Image, error) {
	r := api.ImageFindRequest{}

	d, err := c.redctlClient.ImageFind(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return d.GetImages(), nil
}
