// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"context"

	"gitlab.com/redfield/redctl/api"
)

func (c *Client) SubscribeEvents() (api.Redctl_SubscribeEventsClient, error) {
	r := &api.SubscribeEventsRequest{}

	// Do not use the client's context - this is a streaming RPC
	// so we do not want a timeout.
	return c.redctlClient.SubscribeEvents(context.Background(), r)
}
