// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"errors"
	"fmt"
	"os"

	"github.com/jaypipes/ghw"
	"gitlab.com/redfield/redctl/api"
)

// DomainFind to find a domain
func (c *Client) DomainFind(uuid string) (*api.Domain, error) {
	r := api.DomainFindRequest{
		Uuid: uuid,
	}

	d, err := c.redctlClient.DomainFind(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return d.GetDomain(), nil
}

// DomainFindAll to find all domains
func (c *Client) DomainFindAll() ([]*api.Domain, error) {
	r := api.DomainFindAllRequest{}

	d, err := c.redctlClient.DomainFindAll(c.Context(), &r)
	if err != nil {
		return nil, err
	}

	return d.GetDomains(), nil
}

// DomainFindByUUIDThenName provides single result with prioritization given toward uuid
func (c *Client) DomainFindByUUIDThenName(uuid string, name string) (*api.Domain, error) {
	domains, err := c.DomainFindByUUIDOrName(uuid, name)
	if err != nil {
		return nil, err
	}

	// Given prioritization order of DomainFindByUUIDOrName,
	// we can simply just return with first result.
	if len(domains) > 0 {
		return domains[0], nil
	}

	// no match, no error
	return nil, nil
}

// DomainFindByUUIDOrName provides all matches for uuid or name, with
// prioritization given to uuid matches in the results.
func (c *Client) DomainFindByUUIDOrName(uuid string, name string) ([]*api.Domain, error) {
	domains, err := c.DomainFindAll()
	if err != nil {
		return nil, err
	}

	results := make([]*api.Domain, 0)

	// first scan by uuid
	for _, domain := range domains {
		if domain.GetUuid() == uuid {
			results = append(results, domain)
		}
	}

	// then scan by name
	for _, domain := range domains {
		if domain.GetConfig().GetName() == name {
			results = append(results, domain)
		}
	}

	return results, nil
}

// DomainUpdate to update a domain
func (c *Client) DomainUpdate(domain *api.Domain) error {
	r := api.DomainUpdateRequest{
		Domain: domain,
	}
	_, err := c.redctlClient.DomainUpdate(c.Context(), &r)

	return err
}

// DomainStart to start domain
func (c *Client) DomainStart(uuid string) error {
	if uuid == "" {
		return errors.New("domain uuid required")
	}

	r := api.DomainStartRequest{Uuid: uuid}
	_, err := c.redctlClient.DomainStart(c.Context(), &r)

	return err
}

// DomainStop to stop a domain
func (c *Client) DomainStop(uuid string) error {
	if uuid == "" {
		return errors.New("domain uuid required")
	}

	r := api.DomainStopRequest{Uuid: uuid}
	_, err := c.redctlClient.DomainStop(c.Context(), &r)

	return err
}

// DomainStopAll to stop all running domains.
func (c *Client) DomainStopAll() error {
	r := api.DomainStopAllRequest{}
	_, err := c.redctlClient.DomainStopAll(c.Context(), &r)

	return err
}

// DomainRestart to restart a domain
func (c *Client) DomainRestart(uuid string) error {
	if uuid == "" {
		return errors.New("domain uuid required")
	}

	r := api.DomainRestartRequest{Uuid: uuid}
	_, err := c.redctlClient.DomainRestart(c.Context(), &r)

	return err
}

// DomainCreate to create new domain
func (c *Client) DomainCreate(dom *api.Domain) (*api.Domain, error) {
	r := api.DomainCreateRequest{Domain: dom}
	d, err := c.redctlClient.DomainCreate(c.Context(), &r)
	if err != nil {
		return dom, err
	}

	return d.GetDomain(), nil
}

// DomainRemove to remove domain
func (c *Client) DomainRemove(dom *api.Domain) error {
	r := api.DomainRemoveRequest{Uuid: dom.Uuid}
	_, err := c.redctlClient.DomainRemove(c.Context(), &r)
	return err
}

// DomainHotplugNetworkAttach to attach network device to a domain
func (c *Client) DomainHotplugNetworkAttach(domain *api.Domain, network *api.DomainNetwork) error {
	r := api.DomainHotplugNetworkAttachRequest{
		Domain:  domain,
		Network: network,
	}
	_, err := c.redctlClient.DomainHotplugNetworkAttach(c.Context(), &r)

	return err
}

// DomainHotplugNetworkDetach to detach network device to a domain
func (c *Client) DomainHotplugNetworkDetach(domain *api.Domain, network *api.DomainNetwork) error {
	r := api.DomainHotplugNetworkDetachRequest{
		Domain:  domain,
		Network: network,
	}
	_, err := c.redctlClient.DomainHotplugNetworkDetach(c.Context(), &r)

	return err
}

// DomainHotplugPCIAttach to attach a pci device to a domain
func (c *Client) DomainHotplugPCIAttach(uuid, name string, pci *api.DomainPciDevice) error {
	if uuid == "" && name == "" {
		return errors.New("domain name or uuid required")
	}

	r := api.DomainHotplugPciAttachRequest{
		Uuid:   uuid,
		Pcidev: pci,
	}
	_, err := c.redctlClient.DomainHotplugPciAttach(c.Context(), &r)

	return err
}

// DomainHotplugPCIDetach to detach a pci device to a domain
func (c *Client) DomainHotplugPCIDetach(uuid, name string, pci *api.DomainPciDevice) error {
	if uuid == "" && name == "" {
		return errors.New("domain name or uuid required")
	}

	r := api.DomainHotplugPciDetachRequest{
		Uuid:   uuid,
		Pcidev: pci,
	}
	_, err := c.redctlClient.DomainHotplugPciDetach(c.Context(), &r)

	return err
}

// CreateNDVM creates an NDVM
func (c *Client) CreateNDVM(name string, imageName string, assignAll bool) error {
	if name == "" {
		return errors.New("ndvm name required")
	}

	ndvm := api.NewPVDomain(name)

	ndvm, err := c.DomainCreate(ndvm)
	if err != nil {
		return err
	}

	iname := fmt.Sprintf("%v.qcow2", ndvm.GetUuid())
	image, err := c.ImageCreateBacked(iname, 0, imageName)
	if err != nil {
		return err
	}

	// XXX: GO AWAY BRMGMT
	// Add the required vif for brmgmt
	ndvm.AddNetwork(api.NewDomainNetwork("0", "brmgmt", "00:16:3e:00:13:37"))

	// Add disk using new image
	ndvm.AddDisk(api.NewDomainDisk(image.GetPath(), "qcow2"))

	// If assignAll, do greedy assignment of network class devices
	if assignAll {
		os.Setenv("PCIDB_PATH", "/usr/share")
		os.Setenv("PCIDB_LOCAL_ONLY", "true")

		pci, err := ghw.PCI()
		if err != nil {
			return err
		}

		devices := pci.ListDevices()
		if len(devices) == 0 {
			fmt.Println("Couldn't get the list of pci devices.")
			return fmt.Errorf("could not retrieve PCI devices")
		}

		for _, device := range devices {
			if device.Class.ID == "02" &&
				(device.Subclass.ID == "00" || device.Subclass.ID == "80") {
				pci := api.NewDomainPCIDevice(device.Address)
				ndvm.AddPCIDevice(pci)
			}
		}
	}

	// Update the domain
	return c.DomainUpdate(ndvm)
}
