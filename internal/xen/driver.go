// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package xen

import (
	"gitlab.com/redfield/redctl/api"
)

// Driver represents an interface with xen
type Driver interface {
	StartDomain(domain api.Domain) error
	StopDomain(domain api.Domain) error
	StopAllDomains() error
	RestartDomain(domain api.Domain) error
	IsDomainCreated(domain api.Domain) bool
	NetworkAttach(domain api.Domain, network api.DomainNetwork) error
	NetworkDetach(domain api.Domain, network api.DomainNetwork) error
	PCIDeviceAttach(domain api.Domain, pci api.DomainPciDevice) error
	PCIDeviceDetach(domain api.Domain, pci api.DomainPciDevice) error
}
