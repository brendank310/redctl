// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package repository

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"gitlab.com/redfield/redctl/api"
)

func TestGraphicImport(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "graphics")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	data := make([]byte, 256)

	gPNG := &api.Graphic{
		Name: "graphic.png",
		Data: data,
	}
	gSVG := &api.Graphic{
		Name: "graphic.svg",
		Data: data,
	}
	gTXT := &api.Graphic{
		Name: "graphic.txt",
		Data: data,
	}

	if err := repo.Import(gPNG.GetName(), gPNG.GetData()); err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if err := repo.Import(gSVG.GetName(), gSVG.GetData()); err != nil {
		t.Errorf("Unexpected error: %v", err)
	}
	if err := repo.Import(gTXT.GetName(), gTXT.GetData()); err == nil {
		t.Error("Expected error when using unsupported format")
	}

}

func TestGraphicFind(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	f1, err := ioutil.TempFile(graphicsDir, "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f1.Name())
	f1BaseName := filepath.Base(f1.Name())

	f2, err := ioutil.TempFile("", "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f2.Name())
	f2BaseName := filepath.Base(f2.Name())

	if path := repo.Find(f1BaseName); path == "" {
		t.Error("Expected to receive non-empty path")
	}

	if path := repo.Find(f2BaseName); path != "" {
		t.Errorf("Expected empty result but received '%v'", path)
	}
}

func TestGraphicRemove(t *testing.T) {
	graphicsDir, err := ioutil.TempDir("", "")
	if err != nil {
		t.Fatalf("Failed to create temp directory: %v\n", err)
	}
	defer os.RemoveAll(graphicsDir)

	repo := NewGraphicsRepo(graphicsDir)

	f1, err := ioutil.TempFile(graphicsDir, "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f1.Name())
	f1BaseName := filepath.Base(f1.Name())

	f2, err := ioutil.TempFile("", "*.png")
	if err != nil {
		t.Errorf("Failed to create tmp file: %v", err)
	}
	defer os.Remove(f2.Name())
	f2BaseName := filepath.Base(f2.Name())

	if err := repo.Remove(f1BaseName); err != nil {
		t.Errorf("Failed to remove graphic from repo: %v", err)
	}

	if err := repo.Remove(f2BaseName); err == nil {
		t.Errorf("Expected error removing non-existent graphic from repo: %v", f2.Name())
	}
}
