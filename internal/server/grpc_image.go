// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/redfield/redctl/api"
)

func (s *server) ImageCreate(ctx context.Context, in *api.ImageCreateRequest) (*api.ImageCreateReply, error) {
	name := in.GetImage().GetName()
	size := in.GetImage().GetSize()

	image, err := s.imageRepo.Create(name, size)
	if err != nil {
		log.Printf("failed to create image \"%v\": %v\n", name, err)
		return nil, fmt.Errorf("failed to create image %v: %v", name, err)
	}

	log.Printf("created image: %v (%v bytes)\n", name, size)

	return &api.ImageCreateReply{Image: image}, nil
}

func (s *server) ImageCreateBacked(ctx context.Context, in *api.ImageCreateBackedRequest) (*api.ImageCreateBackedReply, error) {
	name := in.GetImage().GetName()
	size := in.GetImage().GetSize()
	backingName := in.GetBackingImage().GetName()

	image, err := s.imageRepo.CreateBacked(name, backingName, size)
	if err != nil {
		log.Printf("failed to create backed image \"%v\": %v (%v bytes - backing file: %v)\n", name, err, size, backingName)
		return nil, fmt.Errorf("failed to create backed image %v: %v (%v bytes - backing file: %v", name, err, size, backingName)
	}

	log.Printf("created backed image: %v (%v bytes - backing file: %v)\n", name, size, backingName)

	return &api.ImageCreateBackedReply{Image: image}, nil
}

func (s *server) ImageFind(ctx context.Context, in *api.ImageFindRequest) (*api.ImageFindReply, error) {
	var images []*api.Image

	name := in.GetName()
	if name == "" {
		var err error
		if images, err = s.imageRepo.FindAll(); err != nil {
			log.Printf("failed to find results: %v %v\n", in, err)
			return nil, fmt.Errorf("failed to find image: %v %v", in, err)
		}
	} else {
		image, err := s.imageRepo.Find(name)
		if err != nil {
			log.Printf("failed to find results: %v %v\n", in, err)
			return nil, fmt.Errorf("failed to find image: %v %v", in, err)
		}

		if image != nil {
			images = append(images, image)
		}
	}

	log.Printf("find image: %+v\n", images)

	return &api.ImageFindReply{Images: images}, nil
}

func (s *server) ImageRemove(ctx context.Context, in *api.ImageRemoveRequest) (*api.ImageRemoveReply, error) {
	name := in.GetName()
	err := s.imageRepo.Remove(name)
	if err != nil {
		log.Printf("failed to remove image \"%v\": %v\n", name, err)
		return nil, fmt.Errorf("failed to remove image %v: %v", name, err)
	}

	log.Printf("removed image: %v\n", name)

	return &api.ImageRemoveReply{}, nil
}
