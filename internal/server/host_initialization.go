// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/redfield/redctl/api"
)

const (
	hostUUID = "00000000-0000-0000-0000-000000000000"
)

func (s *server) initializeHost() error {
	go s.domainEventHandler()

	if err := s.createDom0Entry(); err != nil {
		log.Printf("Failed to create db entry for dom0: %v", err)
		return err
	}

	// Start domains that don't require UI as soon as system is ready.
	// This minimizes the time required to boot a functional NDVM and
	// other backend VMs that may be required for operation.
	go func() {
		err := s.startPVDomains()
		if err != nil {
			log.Printf("Fatal error attempting to start PV domains: %v\n", err)
		}
	}()

	return nil
}

func (s *server) domainEventHandler() {
	el := s.eventPublisher.registerSubscriber()
	defer s.eventPublisher.unregisterSubscriber(el)

	for {
		event := <-el.update

		switch event.GetType() {

		case api.Event_DOMAIN_STARTED:
			err := s.handleEventDomainStarted(event.GetDomain())
			if err != nil {
				log.Print(err)
			}

		case api.Event_NETWORK_ATTACH:
			err := s.handleEventNetworkAttach(event.GetDomain(), event.GetNetwork())
			if err != nil {
				log.Print(err)
			}
		case api.Event_NETWORK_DETACH:
			err := s.handleEventNetworkDetach(event.GetDomain(), event.GetNetwork())
			if err != nil {
				log.Print(err)
			}
		default:
			log.Printf("Event not handled by redctld: %v", event.GetType())
		}
	}
}

func (s *server) attachHostNetwork(host *api.Domain, network *api.DomainNetwork) {
	// Try up to 10 times to attach the network
	for i := 0; i < 10; i++ {
		time.Sleep(1 * time.Second)

		if err := s.domainNetworkAttach(host, network); err == nil {
			log.Printf("Network attached to dom0: %v", network)
			return
		}

		log.Print("Failed to attach network to dom0, retrying in one second...")
	}

	log.Print("Failed to attach network to dom0 after 10 tries")
}

func (s *server) handleEventDomainStarted(domain *api.Domain) error {
	host, err := s.domainRepo.Find(hostUUID)
	if err != nil {
		return err
	}

	for _, n := range host.GetConfig().GetNetworks() {
		if n.GetBackend() != domain.GetConfig().GetName() {
			continue
		}

		go s.attachHostNetwork(host, n)

		// Successfully handled event
		return nil
	}

	// Domain was not a backend
	return nil
}

func (s *server) handleEventNetworkAttach(domain *api.Domain, network *api.DomainNetwork) error {
	// If the dmoain is dom0, create a systemd network entry
	if domain.GetUuid() == hostUUID {
		return createSystemdNetworkEntry(network)
	}

	return nil
}

func (s *server) handleEventNetworkDetach(domain *api.Domain, network *api.DomainNetwork) error {
	// If the dmoain is dom0, remove the systemd network entry
	if domain.GetUuid() == hostUUID {
		return removeSystemdNetworkEntry(network)
	}

	return nil
}

func (s *server) startPVDomains() error {
	domains, err := s.domainRepo.FindAll()
	if err != nil {
		return err
	}

	for _, dom := range domains {
		if !dom.GetStartOnBoot() || dom.GetConfig().GetType() != "pv" {
			continue
		}

		if err := s.domainStartWithDependencies(dom); err != nil {
			// ignore errors starting domains, but log them
			log.Printf("failed to start pv domain %v: %v\n", dom.Config.Name, err)
		}
	}

	return nil
}

func (s *server) createDom0Entry() error {
	// If there is no entry for dom0, create it
	if _, err := s.domainRepo.Find(hostUUID); err != nil {
		d := &api.Domain{
			Uuid: hostUUID,
			Config: &api.DomainConfiguration{
				Name: "Domain-0",
			},
		}
		err := s.domainRepo.Create(d)
		if err != nil {
			return err
		}
	}

	return nil
}

var (
	systemdNetworkEntryName = "20-redfield-%v.network"

	systemdNetworkEntry = `[Match]
MACAddress=%v

[Network]
DHCP=ipv4

[DHCP]
UseDomains=true
`
)

const systemdNetworkDir = "/run/systemd/network"

func createSystemdNetworkEntry(network *api.DomainNetwork) error {
	mac := strings.Replace(network.GetMac(), ":", "", -1)
	name := fmt.Sprintf(systemdNetworkEntryName, mac)

	entry := fmt.Sprintf(systemdNetworkEntry, network.GetMac())
	path := filepath.Join(systemdNetworkDir, name)

	err := ioutil.WriteFile(path, []byte(entry), 0644)
	if err != nil {
		return fmt.Errorf("unable to create network entry: %v", err)
	}

	err = exec.Command("systemctl", "restart", "systemd-networkd").Run()
	if err != nil {
		return fmt.Errorf("unable to restart systemd-networkd: %v", err)
	}

	return nil
}

func removeSystemdNetworkEntry(network *api.DomainNetwork) error {
	mac := strings.Replace(network.GetMac(), ":", "", -1)
	name := fmt.Sprintf(systemdNetworkEntryName, mac)
	path := filepath.Join(systemdNetworkDir, name)

	err := os.Remove(path)
	if err != nil {
		return fmt.Errorf("unable to remove network entry: %v", err)
	}

	return nil
}
