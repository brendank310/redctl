// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"testing"
)

func parseExtractHelper(t *testing.T, spec string, wantdisk int, wantpart int, wantpath string) {
	disk, part, path, err := parseExtractSpecString(spec)
	if err != nil {
		t.Errorf("Failed to parse extract spec: %v\n", spec)
	}

	if disk != wantdisk {
		t.Errorf("Failed to parse extract spec: %v (got disk = %v, want disk = %v)\n", spec, disk, wantdisk)
	}

	if part != wantpart {
		t.Errorf("Failed to parse extract spec: %v (got part = %v, want part = %v)\n", spec, part, wantpart)
	}

	if path != wantpath {
		t.Errorf("Failed to parse extract spec: %v (got path = %v, want path = %v)\n", spec, path, wantpath)
	}

	t.Logf("passed tested for %v\n", spec)
}

func TestParseExtractSpecString(t *testing.T) {
	parseExtractHelper(t, "0,0:/foo", 0, 0, "/foo")
	parseExtractHelper(t, "0,1:/foo", 0, 1, "/foo")
	parseExtractHelper(t, "1,0:/foo", 1, 0, "/foo")
	parseExtractHelper(t, "0,0:/foo/bar", 0, 0, "/foo/bar")
	parseExtractHelper(t, "0:/foo/bar", 0, -1, "/foo/bar")
	parseExtractHelper(t, "1:/foo/bar", 1, -1, "/foo/bar")
	parseExtractHelper(t, "0:/foo", 0, -1, "/foo")
}
