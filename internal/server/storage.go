// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"log"
	"os"

	"path/filepath"
)

// StorageInitialize initializes storage, creating directories as necessary
// It returns error encountered, if any.
func StorageInitialize(opts *RedctlServerOptions) error {
	log.Printf("using server path: %s\n", opts.ServerPath)

	err := os.MkdirAll(opts.ServerPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.ServerPath, err)
		return err
	}

	opts.DbPath = filepath.Join(opts.ServerPath, "/db")
	err = os.MkdirAll(opts.DbPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.DbPath, err)
		return err
	}

	opts.ImagesPath = filepath.Join("/storage", "images")
	err = os.MkdirAll(opts.ImagesPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.ImagesPath, err)
		return err
	}

	opts.GraphicsPath = filepath.Join("/storage", "graphics")
	err = os.MkdirAll(opts.GraphicsPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.GraphicsPath, err)
		return err
	}

	opts.DataDir = "/usr/share/redfield"

	return nil
}
