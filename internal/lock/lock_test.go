// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package lock

import (
	"testing"
)

func TestLock(t *testing.T) {
	locked, err := TryLock()
	if err != nil {
		t.Fatalf("TryLock failed: %s\n", err.Error())
	}
	if !locked {
		t.Fatalf("TryLock failed to take lock\n")
	}
	if locked != Locked() {
		t.Fatalf("locked != Locked()\n")
	}
	locked, err = Lock(true)
	if err != nil {
		t.Fatalf("Unable to take lock?\n")
	}
	if !locked {
		t.Fatalf("TryLock failed to take lock\n")
	}
	if locked != Locked() {
		t.Fatalf("locked != Locked()\n")
	}
	err = Unlock()
	if err != nil {
		t.Fatalf("Unable to unlock?\n")
	}
	if Locked() {
		t.Fatalf("Failed to unlock?\n")
	}
}
