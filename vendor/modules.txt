# github.com/fsnotify/fsnotify v1.4.7
github.com/fsnotify/fsnotify
# github.com/ghodss/yaml v1.0.0
github.com/ghodss/yaml
# github.com/gofrs/flock v0.7.1
github.com/gofrs/flock
# github.com/golang/protobuf v1.3.0
github.com/golang/protobuf/proto
github.com/golang/protobuf/ptypes/timestamp
github.com/golang/protobuf/protoc-gen-go/descriptor
github.com/golang/protobuf/ptypes
github.com/golang/protobuf/ptypes/any
github.com/golang/protobuf/ptypes/duration
# github.com/google/uuid v1.1.1
github.com/google/uuid
# github.com/hashicorp/hcl v1.0.0
github.com/hashicorp/hcl
github.com/hashicorp/hcl/hcl/printer
github.com/hashicorp/hcl/hcl/ast
github.com/hashicorp/hcl/hcl/parser
github.com/hashicorp/hcl/hcl/token
github.com/hashicorp/hcl/json/parser
github.com/hashicorp/hcl/hcl/scanner
github.com/hashicorp/hcl/hcl/strconv
github.com/hashicorp/hcl/json/scanner
github.com/hashicorp/hcl/json/token
# github.com/inconshreveable/mousetrap v1.0.0
github.com/inconshreveable/mousetrap
# github.com/jaypipes/ghw v0.0.0-20190131082148-032dfb1f8cb2
github.com/jaypipes/ghw
# github.com/jaypipes/pcidb v0.0.0-20190216134740-adf5a9192458
github.com/jaypipes/pcidb
# github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25
github.com/jcelliott/lumber
# github.com/magiconair/properties v1.8.0
github.com/magiconair/properties
# github.com/mitchellh/go-homedir v1.1.0
github.com/mitchellh/go-homedir
# github.com/mitchellh/mapstructure v1.1.2
github.com/mitchellh/mapstructure
# github.com/nanobox-io/golang-scribble v0.0.0-20180621225840-336beac0a992
github.com/nanobox-io/golang-scribble
# github.com/pelletier/go-toml v1.2.0
github.com/pelletier/go-toml
# github.com/pkg/errors v0.8.1
github.com/pkg/errors
# github.com/rs/xid v1.2.1
github.com/rs/xid
# github.com/spf13/afero v1.1.2
github.com/spf13/afero
github.com/spf13/afero/mem
# github.com/spf13/cast v1.3.0
github.com/spf13/cast
# github.com/spf13/cobra v0.0.3
github.com/spf13/cobra
# github.com/spf13/jwalterweatherman v1.0.0
github.com/spf13/jwalterweatherman
# github.com/spf13/pflag v1.0.3
github.com/spf13/pflag
# github.com/spf13/viper v1.3.1
github.com/spf13/viper
# golang.org/x/crypto v0.0.0-20181203042331-505ab145d0a9
golang.org/x/crypto/ssh/terminal
# golang.org/x/net v0.0.0-20180906233101-161cd47e91fd
golang.org/x/net/context
golang.org/x/net/trace
golang.org/x/net/internal/timeseries
golang.org/x/net/http2
golang.org/x/net/http2/hpack
golang.org/x/net/http/httpguts
golang.org/x/net/idna
# golang.org/x/sys v0.0.0-20181205085412-a5c9d58dba9a
golang.org/x/sys/unix
golang.org/x/sys/windows
# golang.org/x/text v0.3.0
golang.org/x/text/transform
golang.org/x/text/unicode/norm
golang.org/x/text/secure/bidirule
golang.org/x/text/unicode/bidi
# google.golang.org/genproto v0.0.0-20180831171423-11092d34479b
google.golang.org/genproto/googleapis/rpc/status
# google.golang.org/grpc v1.19.0
google.golang.org/grpc
google.golang.org/grpc/credentials
google.golang.org/grpc/metadata
google.golang.org/grpc/reflection
google.golang.org/grpc/balancer
google.golang.org/grpc/balancer/roundrobin
google.golang.org/grpc/codes
google.golang.org/grpc/connectivity
google.golang.org/grpc/encoding
google.golang.org/grpc/encoding/proto
google.golang.org/grpc/grpclog
google.golang.org/grpc/internal
google.golang.org/grpc/internal/backoff
google.golang.org/grpc/internal/binarylog
google.golang.org/grpc/internal/channelz
google.golang.org/grpc/internal/envconfig
google.golang.org/grpc/internal/grpcrand
google.golang.org/grpc/internal/grpcsync
google.golang.org/grpc/internal/transport
google.golang.org/grpc/keepalive
google.golang.org/grpc/naming
google.golang.org/grpc/peer
google.golang.org/grpc/resolver
google.golang.org/grpc/resolver/dns
google.golang.org/grpc/resolver/passthrough
google.golang.org/grpc/stats
google.golang.org/grpc/status
google.golang.org/grpc/tap
google.golang.org/grpc/credentials/internal
google.golang.org/grpc/reflection/grpc_reflection_v1alpha
google.golang.org/grpc/balancer/base
google.golang.org/grpc/binarylog/grpc_binarylog_v1
google.golang.org/grpc/internal/syscall
# gopkg.in/yaml.v2 v2.2.2
gopkg.in/yaml.v2
# howett.net/plist v0.0.0-20181124034731-591f970eefbb
howett.net/plist
